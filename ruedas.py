import pigpio
pi = pigpio.pi()

class Wheels():
    #Class constants
    WHEEL1_PIN = 18
    WHEEL2_PIN = 19
    RANGE = 8
    STOPPED = 15
    MAX = 23
    MIN = 7
    SENSIBLITY = 0.3

    """Tank wheels info"""
    def __init__(self, wheel1, wheel2):
        self.wheel1 = wheel1
        self.wheel2 = wheel2

    def InitializeWheels (self):
        #Wheels initially stopped
        self.wheel1 = Wheels.STOPPED
        self.wheel2 = Wheels.STOPPED

        #PWM HW signals configuration
        #WHEEL 1
        pi.set_mode(Wheels.WHEEL1_PIN,pigpio.OUTPUT)
        pi.set_PWM_frequency(Wheels.WHEEL1_PIN,)
        pi.set_PWM_range()
        pi.set_PWM_dutycicle(Wheels.WHEEL1_PIN,Wheels.STOPPED)
        #WHEEL 2
        pi.set_mode(Wheels.WHEEL2_PIN,pigpio.OUTPUT)
        pi.set_PWM_frequency(Wheels.WHEEL2_PIN,)
        pi.set_PWM_range()
        pi.set_PWM_dutycicle(Wheels.WHEEL2_PIN,Wheels.STOPPED)


    def Movimiento(self):
        if posX > Wheels.SENSIBLITY:
            posX = (posX - Wheels.SENSIBLITY)/(1 - Wheels.SENSIBLITY)
        elif posX < Wheels.SENSIBLITY:
            posX = (posX + Wheels.SENSIBLITY)/(1 - Wheels.SENSIBLITY)
        else:
            posX = 0.0

        if posY > Wheels.SENSIBLITY:
            posY = (posY - Wheels.SENSIBLITY)/(1 - Wheels.SENSIBLITY)
        elif posY < Wheels.SENSIBLITY:
            posY = (posY + Wheels.SENSIBLITY)/(1 - Wheels.SENSIBLITY)
        else:
            posY = 0.0

        #Calc PWM signal length for each wheel
        self.wheel1 = Wheels.STOPPED + Wheels.RANGE*(posX - posY)
        self.wheel2 = Wheels.STOPPED + Wheels.RANGE*(posX + posY)

        #We make sure the values dont surpass the max and min
        if self.wheel1 > Wheels.MAX:
            self.wheel1 = Wheels.MAX
        elif self.wheel1 < Wheels.MIN:
            self.wheel1 = Wheels.MIN

        if self.wheel2 > Wheels.MAX:
            self.wheel2 = Wheels.MAX
        elif self.wheel2 < Wheels.MIN:
            self.wheel2 = Wheels.MIN

        #We pass the values to the servos
        pi.set_PWM_dutycicle(Wheels.WHEEL1_PIN,self.wheel1)
        pi.set_PWM_dutycicle(Wheels.WHEEL2_PIN,self.wheel2)
