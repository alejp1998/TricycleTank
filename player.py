# Program to demonstrate
# timer objects in python

import threading
def gfg():
    print("GeeksforGeeks\n")

timer = threading.Timer(2.0, gfg)
timer.start()
print("Exit\n")
#If we want to stop the timer before it ends timer.cancel()

from fysom import Fysom

class Efecto():
    """Efects to play"""
    def __init__(self, name, frecuencies, durations):
        self.name = name
        self.frecuencies = frecuencies
        self.durations = durations


class Player():
    """Player of the efects"""
    def __init__(self):
        self.note = 0
        self.actualfrecuency = 0
        self.actualduration = 0
        self.efecto = None

fsm_player = Fysom({'initial': 'wait_start',
                    'events': [
                        {'name': initialize_play_shoot, 'src': wait_start,  'dst': wait_next},
                        {'name': initialize_play_impact, 'src': wait_start, 'dst': wait_next},
                        {'name': initialize_play_efect, 'src': wait_start, 'dst': wait_next},
                        {'name': initialize_play_impact, 'src': wait_next, 'dst': wait_next},
                        {'name': update_player, 'src': wait_next, 'dst': wait_end},
                        {'name': end_effect, 'src': wait_end, 'dst': wait_start},
                        {'name': start_new_note, 'src': wait_end, 'dst': wait_next}]})


#FSM FUNCTIONS
def initialize_play_shoot(self)
